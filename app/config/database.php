<?php

return [
    'type' => 'mysql',
    'host' => 'localhost',
    'name' => 'netpeak',
    'charset' => 'utf8',
    'user' => 'homestead',
    'password' => 'secret'
];