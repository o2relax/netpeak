<?php

function dd($data = '')
{
    dump($data);
    die;
}

function dump($data = '')
{
    echo '<pre>';
    print_r($data);
    echo '</pre>';
}

function url($url = null, array $array = null)
{
    $params = null;
    if ($array) {
        foreach ($array as $k => $v) {
            $params[] = $k . '=' . $v;
        }
    }
    return 'http://' . $_SERVER['HTTP_HOST'] . ($url ? $url : '/') . ($params ? '?' . implode('&', $params) : '');
}

function redirect($url = null)
{
    header('Location: ' . url($url ? $url : '/'));
    return true;
}

function array_only($array, $keys)
{

    $return = [];

    foreach ($keys as $key) {
        if (isset($array[$key])) $return[$key] = $array[$key];
    }

    return $return;

}

function handleException($e)
{
    http_response_code($e->getCode());
    echo $e->getMessage();
    die;
}