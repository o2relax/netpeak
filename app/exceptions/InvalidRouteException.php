<?php

namespace app\exceptions;

use Exception;

class InvalidRouteException extends Exception {

    protected $message = 'Page not found';

    public function __construct() {
        parent::__construct($this->message, 404);
    }

}