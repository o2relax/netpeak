<?php

namespace app\exceptions;

use Exception;

class InvalidTokenException extends Exception {

    protected $message = 'Invalid token';

    public function __construct() {
        parent::__construct($this->message, 500);
    }
}