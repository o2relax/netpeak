<?php

namespace app;

use app\core\app;

define('ROOT_PATH', realpath(__DIR__ . '/../'));
define('APP_PATH', realpath(__DIR__ . '/../app/'));
define('CONFIG_PATH', realpath(APP_PATH . '/config/'));
define('VIEW_PATH', realpath(APP_PATH . '/view/'));

require_once __DIR__. '/../vendor/autoload.php';

require_once __DIR__ . '/core/app.php';

app::init();
app::$kernel->load();