<?php

namespace app\gateways\transformers;

use app\contracts\Transformer;
use Carbon\Carbon;

/**
 * Class DefaultTransformer
 * @package App\Gateways\Transformers
 */
class TwitterPostTransformer extends Transformer
{

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    /**
     * @var  array
     */
    protected $defaultIncludes = [
    ];

    /**
     * @param $item
     * @return mixed
     */
    public function transform($item)
    {

        $item->profile_url = 'https://twitter.com/' . $item->user->screen_name;
        $item->avatar = $item->user->profile_image_url;
        $item->profile_color = $item->user->profile_link_color;
        $item->date = Carbon::parse($item->created_at)->format('D H:m');
        $item->username = $item->user->name;

        return $item;
    }
}
