<?php

namespace app\services;


class TwitterService extends SocialService {

    /**
     * @param int $limit
     * @return array|string
     * @throws \Exception
     */
    public function getLast($limit = null) {

        if(!$limit) {
            $limit = $this->config->get('twitter_limit');
        }

        $url = 'https://api.twitter.com/1.1/search/tweets.json';
        $getfield = '?q=from%3ANasa%20OR%20%23nasa&count=' . $limit;

        $requestMethod = 'GET';
        $twitter = new \TwitterAPIExchange($this->getSettings());

        return json_decode($twitter->setGetfield($getfield)
            ->buildOauth($url, $requestMethod)
            ->performRequest())->statuses;

    }

    public function getSettings() {
        return $this->config->get('twitter');
    }

}