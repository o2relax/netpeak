<?php

namespace app\services;

use app\core\configs;
use app\interfaces\SocialInterface;

abstract class SocialService implements SocialInterface
{

    public $config;

    /**
     * SocialService constructor.
     */
    public function __construct()
    {
        $this->config = new configs();
    }

}