<?php

/* index.html */
class __TwigTemplate_dff18def41465c130036a9c516bf40c28f30ffa883db11813f338d6f50cd2f21 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    <meta name=\"csrf-token\" content=\"";
        // line 7
        echo twig_escape_filter($this->env, ($context["token"] ?? null), "html", null, true);
        echo "\">
    <title>";
        // line 8
        echo twig_escape_filter($this->env, ($context["title"] ?? null), "html", null, true);
        echo "</title>

    <link href=\"//fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900\"  rel=\"stylesheet\" type=\"text/css\">
    <link href=\"/assets/css/styles.css\"  rel=\"stylesheet\" type=\"text/css\">
</head>
<body>

<div class=\"container\">

</div>
</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "index.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 8,  27 => 7,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "index.html", "/home/vagrant/Code/beejee/app/view/templates/index.html");
    }
}
