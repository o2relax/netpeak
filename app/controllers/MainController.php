<?php

namespace app\controllers;

use app\core\Controller;
use app\core\validator;
use app\models\Task;
use app\models\User;
use app\services\TwitterService;
use app\traits\ResponseTrait;
use http\Env\Request;
use Imagine\Gd\Imagine;
use Imagine\Image\Box;
use Imagine\Image\ImageInterface;
use app\gateways\transformers\TwitterPostTransformer;

class MainController extends Controller
{

    use ResponseTrait;

    protected $isAdmin = false;

    /**
     * MainController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->addVar('title', $this->config->get('app'));
        $this->addVar('auth', $this->session->auth());
        $this->addVar('user', $this->session->user());
        $this->isAdmin = $this->session->isAdmin($this->config->get('admin_id'));
        $this->addVar('isAdmin', $this->isAdmin);
    }

    /**
     * @return string
     * @throws \Exception
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function index()
    {

        $twitter = new TwitterService();

        $posts = $this->transform(
            $twitter->getLast($this->config->get('twitter_limit')),
            TwitterPostTransformer::class
        );

        $date = date('Y-m-d H:i:s');

        return $this->view('page.list', [
            'posts' => $posts,
            'date' => $date
        ]);
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function ajaxPosts()
    {

        $twitter = new TwitterService();

        $posts = $this->transform(
            $twitter->getLast($this->config->get('twitter_limit')),
            TwitterPostTransformer::class
        );

        $date = date('Y-m-d H:i:s');

        return json_encode([
                'posts' => $this->view('ajax.posts', [
                        'posts' => $posts
                    ]
                ),
                'date' => $date
            ]
        );

    }

    /**
     * @return bool|string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function login()
    {

        if ($this->session->auth()) {
            return redirect('/');
        }

        $errors = $this->session->get('errors');
        $old = $this->session->get('old');
        $this->session->delete('errors');
        $this->session->delete('old');

        return $this->view('page.login', [
            'old' => $old,
            'errors' => $errors,
        ]);

    }

    /**
     * @return bool
     * @throws \app\exceptions\InvalidTokenException
     */
    public function postLogin()
    {

        $validator = new validator();

        $rules = [
            'login' => 'required',
            'password' => 'required',
        ];

        $messages = [
            'login.required' => 'Login is required',
            'password.required' => 'Password is required'
        ];

        $this->session->put('old', $this->request->post());

        if ($validator->make($this->request->post(), $rules, $messages)) {

            $user = User::makeAuth($this->request->post('login'), md5($this->request->post('password')));

            if (!$user) {
                $this->session->put('errors', [
                    'login' => 'Wrong login or password',
                    'password' => 'Wrong login or password'
                ]);
                return redirect('/main/login');
            }

            $token = md5(rand());

            $this->session->put('auth', true);
            $this->session->put('token', $token);
            $this->session->put('user', [
                'login' => $user['login'],
                'id' => $user['id']
            ]);

            User::updateToken($user['id'], $token);

            $this->session->delete('old');

            return redirect('/');

        } else {
            $this->session->put('errors', $validator->messages());
            return redirect('/main/login');
        }

    }

    /**
     * @return bool
     */
    public function logout()
    {

        $this->session->logout();

        return redirect('/');

    }

    /**
     * @param $id
     * @return bool|string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function edit($id)
    {

        $task = Task::getById($id);

        if (!$task || !$this->isAdmin) {
            return redirect('/');
        }

        $errors = $this->session->get('errors');
        $old = $this->session->get('old');
        $this->session->delete('errors');
        $this->session->delete('old');

        return $this->view('task.edit', [
            'task' => $task,
            'old' => $old,
            'errors' => $errors
        ]);

    }

    /**
     * @param $id
     * @return bool
     * @throws \app\exceptions\InvalidTokenException
     */
    public function postEdit($id)
    {

        if (!$this->isAdmin) {
            return redirect('/');
        }

        $validator = new validator();

        $rules = [
            'text' => 'required',
        ];

        $messages = [
            'text.required' => 'text is required',
        ];

        $this->session->put('old', $this->request->post());

        if ($validator->make($this->request->post(), $rules, $messages)) {

            Task::update($id, array_only($this->request->post(), Task::$columns));

            $this->session->delete('old');

            return redirect('/main/edit/' . $id);
        } else {
            $this->session->put('errors', $validator->messages());
            return redirect('/main/edit/' . $id);
        }

    }

    /**
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */

    public function create()
    {

        $errors = $this->session->get('errors');
        $old = $this->session->get('old');
        $this->session->delete('errors');
        $this->session->delete('old');

        return $this->view('task.create', [
            'old' => $old,
            'errors' => $errors
        ]);

    }

    /**
     * @return bool
     * @throws \app\exceptions\InvalidTokenException
     */
    public function postCreate()
    {

        $validator = new validator();

        $rules = [
            'username' => 'required',
            'email' => 'required|email',
            'text' => 'required',
            'image' => 'image'
        ];

        $messages = [
            'username.required' => 'text is required',
            'email.required' => 'email is required',
            'email.email' => 'email must be email',
            'text.required' => 'text is required',
            'image.image' => 'only png,jpg,gif',
        ];

        $this->session->put('old', $this->request->post());

        $image = $this->request->file('image');

        $input = array_merge($this->request->post(), [
            'image' => $image
        ]);

        if ($validator->make($input, $rules, $messages)) {

            $imagine = new Imagine();
            $size = new Box(...$this->config->get('image_size'));
            $mode = ImageInterface::THUMBNAIL_INSET;

            $image_name = md5(uniqid() . time()) . '.' . pathinfo($image['name'])['extension'];
            $path = WWW_PATH . '/assets/img/' . $image_name;

            $imagine->open($image['tmp_name'])
                ->thumbnail($size, $mode)
                ->save($path);

            $data = array_merge($this->request->post(), [
                'image' => $image_name,
            ]);

            Task::create(array_only($data, Task::$columns));

            $this->session->delete('old');

            return redirect('/');
        } else {
            $this->session->put('errors', $validator->messages());
            return redirect('/main/create/');
        }

    }

}