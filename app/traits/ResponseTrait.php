<?php

namespace app\traits;

/**
 * Trait ResponseTrait
 * @package app\traits
 */
trait ResponseTrait
{

    /**
     * @param $data
     * @param null $transformerName
     * @return mixed
     */
    public function transform($data, $transformerName = null)
    {
        $transformer = new $transformerName;

        return $transformer->response($data);
    }

}
