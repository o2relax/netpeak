<?php

namespace app\models;

use app\core\models;

class User extends models {

    static $table = 'users';

    public static function getAuth($token) {
        $query = self::query("SELECT * FROM " . self::$table . " WHERE token = '" . $token . "'");
        return isset($query[0]) ? $query[0] : false;
    }

    public static function makeAuth($login, $password) {
        $query = self::query("SELECT * FROM " . self::$table . " WHERE login = '" . $login . "' AND password = '" . $password . "'");
        return isset($query[0]) ? $query[0] : false;
    }

    public static function updateToken($user_id, $token) {
        return self::query("UPDATE " . self::$table . " SET token = '" . $token . "' WHERE id = " . $user_id);
    }

}