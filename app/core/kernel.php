<?php

namespace app\core;

use app\exceptions\InvalidRouteException;

class kernel
{

    public $defaultControllerName = 'Main';

    public $defaultActionName = "index";

    public function load()
    {

        list($controllerName, $actionName, $params) = app::$router->parse();
        echo $this->launchAction($controllerName, $actionName, $params);

    }

    public function launchAction($controllerName, $actionName, $params)
    {

        $controllerName = (empty($controllerName) ? $this->defaultControllerName : ucfirst($controllerName)) . 'Controller';

        if(!file_exists(APP_PATH . DIRECTORY_SEPARATOR . 'controllers' . DIRECTORY_SEPARATOR . $controllerName . '.php')){
            throw new InvalidRouteException();
        }

        require_once APP_PATH . DIRECTORY_SEPARATOR . 'controllers' . DIRECTORY_SEPARATOR . $controllerName . '.php';

        if(!class_exists("\\app\\Controllers\\".ucfirst($controllerName))){
            throw new InvalidRouteException();
        }

        $controllerName = "\\app\\controllers\\" . ucfirst($controllerName);

        $controller = new $controllerName;
        $actionName = empty($actionName) ? $this->defaultActionName : $actionName;

        $actionName = explode('-', $actionName);

        $actionName = $actionName[0] . (isset($actionName[1]) ? ucfirst($actionName[1]) : '');

        if (!method_exists($controller, $actionName)){
            throw new InvalidRouteException();
        }

        return $controller->$actionName(...$params);

    }

}