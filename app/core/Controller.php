<?php

namespace app\core;

use Twig_Loader_Filesystem;
use Twig_Environment;
use Twig_Loader_Array;

abstract class Controller {

    public $config = [];
    public $request = [];
    public $session = [];
    protected $twig;
    protected $loader;
    protected $vars = [];

    /**
     * Controller constructor.
     */

    public function __construct()
    {
        $this->request = new requests();
        $this->config = new configs();
        $this->session = new session();
        $this->loader = new Twig_Loader_Filesystem(VIEW_PATH . DIRECTORY_SEPARATOR . $this->config->get('templates'));

        $this->twig = new Twig_Environment($this->loader, array(
            'cache' => VIEW_PATH . DIRECTORY_SEPARATOR . $this->config->get('templates_cache'),
            'debug' => true,
            'auto_reload' => true
        ));

        app::$token = md5($this->request->server('HTTP_HOST') . $this->config->get('secret'));
        $this->addVar('token', app::$token);
        $this->addVar('config', $this->config->all());

    }

    /**
     * @param $key
     * @param $value
     */
    public function addVar($key, $value) {
        $this->vars[$key] = $value;
    }

    /**
     * @param $template
     * @param array $data
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function view($template, array $data = []) {
        $this->twig = new Twig_Environment($this->loader);
        return $this->twig->render(str_replace('.', DIRECTORY_SEPARATOR, $template) . '.html', array_merge($data, $this->vars));
    }

}