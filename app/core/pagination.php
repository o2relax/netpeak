<?php

namespace app\core;

use Twig_Loader_Filesystem;
use Twig_Environment;
use Twig_Loader_Array;

class pagination
{

    protected $twig;
    protected $loader;
    public $config;

    public function __construct()
    {
        $this->config = new configs();
    }

    public function render($count_all, $per_page, $active_page, $get)
    {

        $count_pages = ceil($count_all / $per_page);
        $count_show_pages = 5;
        unset($get['page']);
        $url = url('/', $get);
        $url_page = url('/', array_merge($get, ['page' => '']));
        if ($count_pages > 1) {
            $prev = $active_page - 1;
            $next = $count_pages - $active_page;
            if ($prev < floor($count_show_pages / 2)) $first = 1;
            else $first = $active_page - floor($count_show_pages / 2);
            $last = $first + $count_show_pages - 1;
            if ($last > $count_pages) {
                $first -= ($last - $count_pages);
                $last = $count_pages;
                if ($first < 1) $first = 1;
            }
            $links = [];

            for ($i = $first; $i <= $last; $i++) {
                $links[$i] = $i;
            }

            $this->loader = new Twig_Loader_Array(array(
                'pagination' => file_get_contents(VIEW_PATH . DIRECTORY_SEPARATOR . $this->config->get('templates') . DIRECTORY_SEPARATOR . 'include' . DIRECTORY_SEPARATOR . 'pagination.html'),
            ));

            $this->twig = new Twig_Environment($this->loader);
            return $this->twig->render('pagination',
                compact('active_page', 'count_pages', 'prev', 'next', 'first', 'last', 'url', 'url_page', 'links')
            );

        } else {
            return null;
        }
    }

}