<?php

namespace app\core;

abstract class models {

    static $table;

    public static function query($query) {
        return app::$db->execute($query);
    }

    public static function getById($id) {
        $query = self::query("SELECT * FROM " . static::$table . " WHERE id = '" . $id . "'");
        return isset($query[0]) ? $query[0] : false;
    }

    public static function update($id, array $data = null) {
        $values = [];
        foreach($data as $k => $v) {
            $values[] = $k . "='" . $v . "'";
        }
        return self::query("UPDATE " . static::$table . " SET " . implode(', ', $values) . " WHERE id = '" . $id . "'");
    }

    public static function create(array $data = null) {
        return self::query("INSERT INTO " . static::$table . " (" . implode(', ', array_keys($data)). ") VALUES ('" . implode('\', \'', array_values($data)). "')");
    }

}