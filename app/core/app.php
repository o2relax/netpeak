<?php

namespace app\core;

use Exception;

class app
{
    /*
     * @var
     */
    public static $router;

    /**
     * @var
     */
    public static $db;

    /**
     * @var
     */
    public static $kernel;

    /**
     * @var
     */
    public static $token;

    /**
     *
     */
    public static function init()
    {
        spl_autoload_register(['static', 'loadClass']);
        static::bootstrap();
        set_exception_handler('handleException');
    }

    /**
     *
     */

    public static function bootstrap()
    {
        session_start();
        static::$router = new router();
        static::$kernel = new kernel();
        static::$db = new db();

    }

    /**
     * @param $className
     */

    public static function loadClass($className)
    {
        $className = str_replace('\\', DIRECTORY_SEPARATOR, $className);
        require_once ROOT_PATH . DIRECTORY_SEPARATOR . $className . '.php';
    }

}